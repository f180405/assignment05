package com.example.assignment05;

public class WeatherInfo {
    public String name;
    public static class Main {
        public static float temp, humidity;
    }
    public class Weather {
        public String description;
    }
    public static class Wind {
        public float speed, deg;
    }
    static public class Sys {
        public float country;
    }
};