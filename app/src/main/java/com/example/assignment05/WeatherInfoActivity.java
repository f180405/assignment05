package com.example.assignment05;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class WeatherInfoActivity extends AppCompatActivity {

    Button history;
    TextView city, country_v, temp_v, humidity_v, wind_speed, wind_dir, description_v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_info);

        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "weather").allowMainThreadQueries().build();

        Intent i = getIntent();

        String CITY = i.getStringExtra("city");
        String API_KEY = "76ab223e952519bd14cdfa1ff810ba25";
        String url = "https://api.openweathermap.org/data/2.5/weather?q=" + CITY + "&appid=" + API_KEY + "&units=metric";

        Log.i("url", url);

        city = findViewById(R.id.textView12);
        country_v = findViewById(R.id.textView13);
        temp_v = findViewById(R.id.textView14);
        description_v = findViewById(R.id.textView15);
        wind_speed = findViewById(R.id.textView16);
        wind_dir = findViewById(R.id.textView17);
        humidity_v = findViewById(R.id.textView18);

        RequestQueue queue = Volley.newRequestQueue(this);

        WeatherInfoEntity weather = new WeatherInfoEntity();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String temp = response.getJSONObject("main").getString("temp");
                            String humidity = response.getJSONObject("main").getString("humidity");
                            String speed = response.getJSONObject("wind").getString("speed");
                            String dir = response.getJSONObject("wind").getString("deg");
                            JSONObject jArray = (JSONObject) response.getJSONArray("weather").get(0);
                            String description = jArray.getString("description");
                            String country = response.getJSONObject("sys").getString("country");

                            city.setText(CITY);
                            temp_v.setText(temp + " degree");
                            country_v.setText(country);
                            description_v.setText(description);
                            humidity_v.setText(humidity);
                            wind_dir.setText(dir + " degree");
                            wind_speed.setText(speed + "m/s");

                            weather.CityName = CITY;
                            weather.temperature = temp;
                            weather.description = description;
                            weather.windDir = dir;
                            weather.windSpeed = speed;
                            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                            Date date = new Date();
                            weather.recordDate = formatter.format(date);
                            weather.humidity = humidity;

                            WeatherInfoDao weatherD = db.weatherInfoDao();

                            weatherD.insertAll(weather);

                            Log.i("TEMP: ", temp);

//                            Toast.makeText(getApplicationContext(), temp, Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Toast.makeText(getApplicationContext(), "Error occurred!", Toast.LENGTH_SHORT).show();
                    }
                });
        queue.add(jsonObjectRequest);

        history = findViewById(R.id.button2);

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), WeatherHistoryActivity.class);
                startActivity(i);
            }
        });

        CityDao cityD = db.cityDao();
        CityEntity c = new CityEntity();

        List<CityEntity> city = cityD.findCityWithName(CITY.toLowerCase(Locale.ROOT));

        if (city.size() <= 0) {
            c.cityName = CITY.toLowerCase(Locale.ROOT);
            cityD.insertAll(c);
        }
    }
}