package com.example.assignment05;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CityWeatherHistoryActivity extends AppCompatActivity {

    TextView heading;
    RecyclerView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_weather_history);

        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "weather").allowMainThreadQueries().build();

        List<WeatherInfoEntity> weatherInfo;

        WeatherInfoDao weatherD = db.weatherInfoDao();

        Intent i = getIntent();

        String cityName = i.getStringExtra("city_name");

        weatherInfo = weatherD.findWeatherWithCity(cityName);

        Log.d("TAG", cityName);

        for (int j = 0; j < weatherInfo.size(); j++) {
            Log.d("CITY", weatherInfo.get(j).CityName);
            Log.d("TEMP", weatherInfo.get(j).temperature);
            Log.d("DATE", weatherInfo.get(j).recordDate);
        }

        list = findViewById(R.id.weatherList);
        list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        WeatherHistoryAdapter adapter = new WeatherHistoryAdapter(weatherInfo);
        list.setAdapter(adapter);

        heading = findViewById(R.id.textView19);

        heading.setText("Weather history of " + cityName);

        adapter.notifyDataSetChanged();


    }
}