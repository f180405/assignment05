package com.example.assignment05;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class WeatherHistoryAdapter extends RecyclerView.Adapter<WeatherHistoryAdapter.ViewHolder> {

    private List<WeatherInfoEntity> weatherInfoList;

    public WeatherHistoryAdapter(List<WeatherInfoEntity> weatherInfoList) {
        this.weatherInfoList = weatherInfoList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listview_recycle_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.temerature.setText(weatherInfoList.get(position).temperature);
        holder.description.setText(weatherInfoList.get(position).description);
        holder.humidity.setText(weatherInfoList.get(position).humidity);
        holder.windDirection.setText(weatherInfoList.get(position).windDir);
        holder.windSpeed.setText(weatherInfoList.get(position).windSpeed);
        holder.recordDate.setText(weatherInfoList.get(position).recordDate);
    }


    @Override
    public int getItemCount() {
        return weatherInfoList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView temerature, recordDate, humidity, windSpeed, windDirection, description;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            temerature = (TextView) itemView.findViewById(R.id.textView23);
            description = (TextView) itemView.findViewById(R.id.textView24);
            humidity = (TextView) itemView.findViewById(R.id.textView25);
            windSpeed = (TextView) itemView.findViewById(R.id.textView28);
            windDirection = (TextView) itemView.findViewById(R.id.textView29);
            recordDate = (TextView) itemView.findViewById(R.id.textView31);
        }

    }
}
