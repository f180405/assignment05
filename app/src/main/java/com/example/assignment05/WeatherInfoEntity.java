package com.example.assignment05;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "weather_info")
public class WeatherInfoEntity {
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "city_name")
    public String CityName;

    @ColumnInfo(name = "record_date")
    public String recordDate;

    @ColumnInfo(name = "temperature")
    public String temperature;

    @ColumnInfo(name = "description")
    public String description;

    @ColumnInfo(name = "wind_speed")
    public String windSpeed;

    @ColumnInfo(name = "wind_dir")
    public String windDir;

    @ColumnInfo(name = "humidity")
    public String humidity;
}
