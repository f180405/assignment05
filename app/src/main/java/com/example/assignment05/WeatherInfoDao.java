package com.example.assignment05;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface WeatherInfoDao {
    @Query("SELECT * FROM weather_info")
    List<WeatherInfoEntity> getAll();

    @Query("SELECT * FROM weather_info WHERE city_name=:city")
    public List<WeatherInfoEntity> findWeatherWithCity(String city);

    @Insert
    void insertAll(WeatherInfoEntity... weatherInfos);
}
