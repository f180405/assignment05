package com.example.assignment05;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class WeatherHistoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_history);

//        String[] mobileArray = {"Android","IPhone","WindowsMobile","Blackberry",
//                "WebOS","Ubuntu","Windows7","Max OS X"};

        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "weather").allowMainThreadQueries().build();

        CityDao cityD = db.cityDao();

        List<CityEntity> cities =  cityD.getAll();

        ArrayList<String> cityNames = new ArrayList<String>();

        ListView cityList = findViewById(R.id.list);

        for (int i = 0; i < cities.size(); i++) {
            cityNames.add(cities.get(i).cityName.substring(0, 1).toUpperCase(Locale.ROOT) + cities.get(i).cityName.substring(1));
        }

        ArrayAdapter adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.litsview_item, cityNames);

        ListView list = findViewById(R.id.list);
        list.setAdapter(adapter);

        cityList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView city = (TextView) view;
                String cityName = city.getText().toString();

                Intent intent = new Intent(getApplicationContext(), CityWeatherHistoryActivity.class);
                intent.putExtra("city_name", cityName);
                startActivity(intent);

            }
        });
    }
}